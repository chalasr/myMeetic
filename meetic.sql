-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 08 Août 2014 à 20:09
-- Version du serveur: 5.5.38-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `meetic`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `created` datetime NOT NULL,
  `envoyeur_id` varchar(60) NOT NULL,
  `receveur_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id_comment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `comment`
--

INSERT INTO `comment` (`id_comment`, `username`, `email`, `content`, `created`, `envoyeur_id`, `receveur_id`, `parent_id`) VALUES
(2, 'root', 'user@user.com', 'Pu**n mais qui c''est ce bo-goss ?! ', '2014-06-21 14:26:16', 'post', 10, 0);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `message` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `email`, `message`, `created`, `name`) VALUES
(29, 'robin@robin.com', 'Mon premier vrai message', '2014-06-19 19:36:20', 'Robin'),
(30, 'chalas_r@epitech.eu', 'lol', '2014-06-19 19:37:15', 'gnome'),
(31, 'roubzon.roubzon@roubzon', 'Le roubz talien', '2014-06-19 19:53:24', 'Roubzon');

-- --------------------------------------------------------

--
-- Structure de la table `medias`
--

CREATE TABLE IF NOT EXISTS `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT 'avatar',
  `file` varchar(255) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_medias_posts_idx` (`post_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Contenu de la table `medias`
--

INSERT INTO `medias` (`id`, `name`, `file`, `post_id`, `type`, `user_id`) VALUES
(59, 'userdefault', '2014-07/user.png', 0, 'img', 0),
(82, 'glyph', '65/glyphicons-halflings.png', 0, 'img', 65),
(62, 'logotest', '65/logo.png', 0, 'img', 65),
(83, 'trash', '72/trash.png', 0, 'img', 72),
(84, 'testhomme', '72/homme2.png', 0, 'img', 72),
(86, 'testavatar', '1/homme.png', 0, 'img', 1),
(85, 'testlogo', '1/homme2.png', 0, 'img', 1),
(80, 'imgcouple', '65/img-couple.png', 0, 'img', 65),
(81, 'trashtest', '65/trash.png', 0, 'img', 65),
(90, NULL, '1/femme.png', 0, 'img', 1),
(91, NULL, '1/arrow-bottom.PNG', 0, 'img', 1),
(92, 'monavatar', '1/avatar.png', 0, 'img', 1),
(93, 'name', '1/avatar.png', 0, 'img', 1),
(95, 'root', '1/trash.png', 0, 'img', 1),
(96, 'root', '1/avatar.png', 0, 'img', 1),
(97, 'rootavat', '1/avatar.png', 0, 'img', 1),
(98, 'gnome', '1/moi1.jpg', 0, 'img', 1),
(99, 'HTML5', '1/planning.png', 0, 'img', 1);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `sendername` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `created` datetime NOT NULL,
  `envoyeur_id` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`id`, `username`, `sendername`, `content`, `created`, `envoyeur_id`) VALUES
(3, 'root', 'Chrissy69', 'Salut root ! je ferais bien connaissance avec toi quand même !Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, nihil, facilis! Numquam voluptatibus aliquam voluptatem repellendus ab deserunt, voluptate blanditiis ex adipisci delectus veritatis itaque consectetur repellat tempore! Libero, quasi.\r\n', '2014-08-04 22:06:21', '74'),
(19, 'Cecile74', 'root', 'Salut ma cécile !', '2014-08-05 13:49:30', '1'),
(20, 'Chrissy69', 'root', 'salut christelle', '2014-08-05 13:57:51', '1'),
(23, 'Stefan26', 'root', 'wesh token !!', '2014-08-05 16:06:50', '1'),
(24, 'Joseph', 'root', 'Salut Joseph !', '2014-08-05 16:24:28', '1');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `created` datetime DEFAULT NULL,
  `online` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tags` set('php','html','css','sql','javascript') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_posts_users1_idx` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`id`, `name`, `content`, `created`, `online`, `type`, `slug`, `user_id`, `tags`) VALUES
(10, 'Mon premier article', '<img src="/Projet_Web_my_weblog/Blog/webroot/img/2014-06/moi1.jpg" alt="image" />\r\n<p>Voici le premier article de ce log !</p>', '2014-06-05 00:00:00', 1, 'post', 'article-11', 1, 'html'),
(12, 'Mon deuxème article', '<h4> Présentation de la Web@cadémie !</h4>\r\n<object width="435" height="280" data="https://www.youtube.com/embed/8ie4xc385Gg"></object>', NULL, 1, 'post', 'article-2', NULL, 'php'),
(14, 'PHP', '<img src="/Projet_Web_my_weblog/Blog/webroot/img/2014-06/php.jpg" alt="image" />\r\n<p>PHP: Hypertext Preprocessor, plus connu sous son sigle PHP (acronyme récursif), est un langage de programmation libre4 principalement utilisé pour produire des pages Web dynamiques via un serveur HTTP3, mais pouvant également fonctionner comme n''importe quel langage interprété de façon locale. PHP est un langage impératif orienté objet.\r\n</p>', '2014-06-11 00:00:00', 1, 'post', 'article-php', NULL, 'php'),
(17, 'HTML5', '<img src="/Projet_Web_my_weblog/Blog/webroot/img/2014-06/html-5.jpg" alt="image" />\r\n<p><b>HTML5 (HyperText Markup Language 5)</b> est la dernière révision majeure d''HTML .<br/>. Le langage comprend également une couche application avec de nombreuses API, ainsi qu''un algorithme afin de pouvoir traiter les documents à la syntaxe non conforme. Le travail a été repris par le W3C en mars 2007 après avoir été lancé par le WHATWG . Le W3C vise la clôture des ajouts de fonctionnalités le 22 mai 2011 et une finalisation de la spécification en 20141, et encourage les développeurs Web à utiliser HTML 5 dès maintenant. </p>', '2014-06-21 00:00:00', 1, 'post', 'article-html', NULL, 'html'),
(18, 'Javascript', '<img src="/Projet_Web_my_weblog/Blog/webroot/img/2014-06/Javascript.png" alt="image" />\r\n<p>JavaScript (souvent abrégé JS) est un langage de programmation de scripts principalement utilisé dans les pages web interactives mais aussi côté serveur2. C’est un langage orienté objet à prototype, c’est-à-dire que les bases du langage et ses principales interfaces sont fournies par des objets qui ne sont pas des instances de classes, mais qui sont chacun équipés de constructeurs permettant de créer leurs propriétés, et notamment une propriété de prototypage qui permet d’en créer des objets héritiers personnalisés. En outre, les fonctions sont des objets de première classe.</p>', '2014-06-21 00:00:00', 1, 'post', 'article-js', NULL, 'javascript'),
(19, 'MySQL', '<img src="/Projet_Web_my_weblog/Blog/webroot/img/2014-06/mysql.png" alt="image" />\r\n<p>MySQL est un système de gestion de bases de données relationnelles (SGBDR). Il est distribué sous une double licence GPL et propriétaire. Il fait partie des logiciels de gestion de base de données les plus utilisés au monde1, autant par le grand public (applications web principalement) que par des professionnels, en concurrence avec Oracle, Informix et Microsoft SQL Server.</p>\r\n\r\n<p>Son nom vient du prénom de la fille du cocréateur Michael Widenius, My. SQL fait allusion au Structured Query Language, le langage de requête utilisé.</p>', '2014-06-21 00:00:00', 1, 'post', 'article-sql', NULL, 'sql'),
(20, 'Mon article test', 'Salut ! nouvel article', '2014-08-08 00:00:00', 1, 'post', 'articletest', NULL, 'php');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(60) NOT NULL,
  `prenom` varchar(60) NOT NULL,
  `naissance` date NOT NULL,
  `tranche` int(11) NOT NULL,
  `sexe` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `actif` int(11) NOT NULL DEFAULT '0',
  `role` varchar(60) NOT NULL DEFAULT 'user',
  `ville` varchar(255) NOT NULL,
  `pays` varchar(60) NOT NULL,
  `region` varchar(60) NOT NULL,
  `departement` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `pseudo` (`pseudo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `password`, `nom`, `prenom`, `naissance`, `tranche`, `sexe`, `email`, `actif`, `role`, `ville`, `pays`, `region`, `departement`) VALUES
(1, 'root', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'Chalas', 'Robin', '1994-03-27', 1, 0, 'robin.chalas@epitech.eu', 0, 'user', 'Villeurbanne', 'France', 'Rhône-Alpes', 'Rhône'),
(68, 'test2', '109f4b3c50d7b0df729d299bc6f8e9ef9066971f', 'test2', 'test2', '1993-03-27', 1, 1, 'test1@test2.fr', 0, 'user', 'tzest', 'test', 'test', 'tezst'),
(67, 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test', 'test', '1990-01-01', 1, 0, 'test@test.fr', 0, 'user', 'test', 'test', 'test', 'test'),
(65, 'Roupiiin', 'e8116efe67cded722422392443aee62153aba13e', 'roubin', 'roubin', '1211-02-21', 4, 0, '21@21.fr', 0, 'user', 'fran', 'fran', 'fran', 'fran'),
(66, 'Jonas', '35a2c6fae61f8077aab61faa4019722abf05093c', 'jonas', 'jonas', '1990-01-01', 1, 0, 'jon@jon.fr', 0, 'user', 'jon', 'jon', 'jon', 'jon'),
(69, 'angegardien', '54a7760c92253b888326179ddf0d876f041f8929', 'Angegardien', 'Josephine', '1994-03-27', 1, 1, 'josephine@angegardien.com', 0, 'user', 'Privas', 'France', 'Rhône-Alpes', 'Ardeche'),
(70, 'Mariadu75', '567c04e4f5a3bdb6619cc02bd379b3abe63204c8', 'Alvez', 'Maria', '1990-04-02', 1, 1, 'maria@alvez.com', 0, 'user', 'Paris', 'France', 'île-de-france', 'Paris'),
(71, 'Joseph', '1f71e0f4ac9b47cd93bf269e4017abaab9d3bd63', 'Callet', 'Joseph', '1996-03-27', 1, 0, 'bonjour@bjr.fr', 0, 'user', 'Montélimar', 'France', 'Rhône-Alpes', 'Drôme'),
(72, 'Picholle', '1dcfb1f38abb331c6117ad516f6488c95b60e4c4', 'picholle', 'picholle', '1994-03-20', 1, 0, 'rout@rout.com', 0, 'user', 'Lyon', 'France', 'Rhône-Alpes', 'Rhône'),
(73, 'July69', '23b56a3fc2e099b2326523ffa03b9bf2a36392c8', 'Bourgu', 'Julie', '1993-05-20', 1, 1, 'julie.bourgu@bourgu.com', 0, 'user', 'Grenoble', 'France', 'Rhône-Alpes', 'Isère'),
(74, 'Chrissy69', '98e963230480c5f23893c7b91c5776e839eabae2', 'Dupont', 'Christelle', '1990-02-20', 1, 1, 'chrissy@chrissy.fr', 0, 'user', 'Lyon', 'France', 'Rhône-Alpes', 'Rhône'),
(76, 'tranche', '65049dada199684c0746bb8a850c5017a832c77d', 'tranche', 'test', '1980-01-01', 2, 0, 'test@tranch.fr', 0, 'user', 'tranche', 'France', 'france', 'test'),
(77, 'Cecile74', 'bb1068bed1056aabf854221063d33476ccccb98e', 'Penel', 'Cécile', '1978-01-20', 3, 1, 'penel@cecile.fr', 0, 'user', 'Annecy', 'France', 'Rhône-Alpes', 'Haute-Savoie'),
(78, 'Stefan26', 'd83d531b9a16def64c8137568dea2a5397214249', 'Doumba', 'Stefan', '1990-01-20', 1, 0, 'stefan@doumba.com', 0, 'user', 'Montélimar', 'France', 'Rhône-Alpes', 'Drôme'),
(79, 'Dje', '63bca33a6a1e19f381dec9b7049f8467c9b91a3c', 'Chalas', 'Jerome', '1961-12-20', 4, 0, 'jerome@chalas.fr', 0, 'user', 'Montélimar', 'France', 'Rhône-Alpes', 'Drôme'),
(80, 'Jojo', '13de8889aecf8f48d9c799a1f3fb520fa748372a', 'Agopa', 'Jordy', '1992-12-20', 1, 0, 'jordy@agop.fr', 0, 'user', 'Montélimar', 'France', 'Rhône-Alpes', 'Drôme');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
