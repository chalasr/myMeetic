<?php
class Message extends Model{

    var $validate = array(
        'content' => array(
            'rule' => 'notEmpty',
            'message' => 'Veuillez écrire votre message'
        )
    );
}