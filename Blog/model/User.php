<?php
class User extends Model{

	var $validate = array(
		'pseudo' => array(
			'rule' => 'notEmpty',
			'message' => 'Veuillez choisir un pseudo !'
		),
		'password' => array(
			'rule' => 'notEmpty',
			'message' => "Veuillez choisir un mot de passe !"
		)
	);

	function checkPseudo($pseudo){
		$ligne = $this->findFirst(array('fields' => 'id,pseudo', 'conditions'=> array('pseudo' => $pseudo)));
		return ($ligne !== FALSE)? true: false;
	}

	function checkEmail($email){
		$ligne = $this->findFirst(array('fields' => 'id,email', 'conditions'=> array('email' => $email)));
		return ($ligne !== FALSE)? true: false;
	}

	function age($date_naissance){

		$arr1 = explode('-', $date_naissance);
		$anneetmp = $arr1[0];
		$arr1[0] = $arr1[2];
		$arr1[2] = $anneetmp;
		$arr2 = explode('/', date('d/m/Y'));

		if(($arr1[1] < $arr2[1]) || (($arr1[1] == $arr2[1]) && ($arr1[0] <= $arr2[0])))
			return $arr2[2] - $arr1[2];

		return $arr2[2] - $arr1[2] - 1;
	}


	// // Petit exemple
	// $ma_date_de_naissance = '27-03-1994';
	// $mon_age = Age($ma_date_de_naissance);

	// echo $mon_age;


/**
** RECHERCHE MULTI-CRITÈRES
**/
	public function search(){
	    $sexe = $_POST['sexe'];
	    $tranche = $_POST['tranche'];
	    $postville = substr($_POST['ville'], 0, -1);
	    $villes = preg_split("/[\s;]+/", $postville);
	    $region = $_POST['region'];
	    $departement = $_POST['departement'];
	   	$pays = $_POST['pays'];

	    $req = "SELECT * FROM users WHERE";

		if($sexe == 1){
	    	$req .= " sexe = 1";
	    }elseif($sexe == 2){
	    	$req .= " 	sexe = 0";
		}elseif($sexe == 3){
			$req .= " (sexe = 0 OR sexe = 1)";
		}

		//tranche
		if(isset($tranche) && $tranche < 5)
			$req .= " AND tranche = $tranche";

		//pays
		if(!empty($pays) && $pays === "tous"){
			$req .= "";
		}elseif(!empty($pays) && $pays == "France"){
			$req .= " AND pays = 'France'";
		}elseif(!empty($pays) && $pays === "Autres" ){
			$req .= " AND pays != 'France'";
		}

		//region
		if(!empty($region) && $region === "toutes"){
			$req .= "";
		}elseif(!empty($region)){
			$req .= " AND region = '$region' ";
		}

		//departement
		if(!empty($departement) && $departement === "toutes"){
			$req .= "";
		}elseif(!empty($departement)){
			$req .= " AND departement = '$departement' ";
		}

		//ville
		if(!empty($_POST['ville']) && $postville == "toutes"){
			$req .= "";
		}elseif(isset($_POST['ville']) && !empty($_POST['ville'])){
			foreach($villes as $k => $v){
				if(!is_numeric($v)){
                   $v = $this->db->quote($v);
                }
                $cond[] = $v;
                $implville = implode(',', $cond);
			}
			$req .= " AND ville in ($implville)";
		}

		$req .= " ORDER BY id DESC";
	    $res = $this->db->prepare($req);
	    $res->execute();
	    $result = $res->fetchAll(PDO::FETCH_OBJ);
		if(!empty($result)){
			echo '<div class=\'row listusers\'>';
			echo '<div class="nbrresult">';
			echo '<h2>'.count($result).' résultat(s)</h2>';
			//echo $implville.'<br>'.$req; //affiche la requete;
			echo '</div>';

			foreach ($result as $k => $v ){
				echo '<div class=\'user-index\'>';
				echo '<h2>'.$v->pseudo.'</h2>';
				echo '<div>';
				echo '<div class=\'index-article\'>';
				echo $this->age($v->naissance)." ans";
				echo '<p>'.$v->ville.'</p>';
				echo '</div>';
				echo '<p class="viewprofil"><a href="'.Router::url("cockpit/users/view/$v->id").'">Voir le profil &rarr;</a><p>';
				echo '</div>';
				echo '</div>';
			}
			echo '</div>';
		}else{
			echo '<div class=\'row\'>';
			echo '<h1>Aucun Resultat</h1>';
			echo '</div>';

		}

	}


}