<?php
class Form{

	public $controller;
	public $errors;
	public $textastyle;

	public function __construct($controller){
		$this->controller = $controller;
	}

	public function input($name,$label,$options = array()){
		$this->textastyle = '<div id="format-buttons">
			<input type="button" class="btn btn-default" onclick="insertTag(\'<p>\',\'</p>\',\'inputcontent\');" value="Nouveau paragraphe" />
            <input type="button" class="btn btn-default" onclick="insertTag(\'<b>\',\'</b>\',\'inputcontent\');" value="Gras" />
            <input type="button" class="btn btn-default" onclick="insertTag(\'<i>\',\'</i>\',\'inputcontent\');" value="Italic" />
            <input type="button" class="btn btn-default" onclick="insertTag(\'<u>\',\'</u>\',\'inputcontent\');" value="Souligner" />
            <input type="button" class="btn btn-default" onclick="insertTag(\'<br/>\',\'\',\'inputcontent\');" value="Retour à la ligne" />
            <input type="button" class="btn btn-default" onclick="upload_image();" value="Insérer une image" />
            </div>';
		$error = false;
		$classError = '';
		if(isset($this->errors[$name])){
			$error = $this->errors[$name];
			$classError = ' alert alert-danger';
		}
		if(!isset($this->controller->request->data->$name)){
			$value = '';
		}else{
			$value = $this->controller->request->data->$name;
		}
		if($label == 'hidden'){
			return '<input type="hidden" name="'.$name.'" value="'.$value.'">';
		}
		$html = '<div class="form-group'.$classError.'">
					<label class="col-sm-4" >'.$label.'</label>
					<div class="col-sm-7">';
		$attr = ' ';
		foreach($options as $k=>$v){ if($k!='type'){
			$attr .= " $k=\"$v\"";
		}}
		if(!isset($options['type']) && !isset($options['options'])){
			$html .= '<input placeholder="'.$label.'" class="form-control input'.$name.'" type="text" required name="'.$name.'" value="'.$value.'"'.$attr.'>';
		}elseif(isset($options['options'])){
			$html .= '<select id="'.$name.'" name="'.$name.'">';
			foreach($options['options'] as $k=>$v){
				$html .= '<option value="'.$k.'" '.($k==$value?'selected':'').'>'.$v.'</option>';
			}
			$html .= '</select>';
		}elseif($options['type'] == 'textarea'){
			$html .= $this->textastyle.'<textarea id="'.$name.'" name="'.$name.'"'.$attr.'>'.$value.'</textarea>';
		}elseif($options['type'] == 'checkbox'){
			$html .= '<input class="form-control" type="hidden" name="'.$name.'" value="0"><input type="checkbox" name="'.$name.'" value="1" '.(empty($value)?'':'checked').'>';
		}elseif($options['type'] == 'file'){
			$html .= '<input class="form-control" type="file" class="input-file" id="'.$name.'" name="'.$name.'"'.$attr.'>';
		}elseif($options['type'] == 'password'){
			$html .= '<input placeholder="'.$label.'" class="form-control '.$name.'" type="password" required name="'.$name.'" value="'.$value.'"'.$attr.'>';
		}elseif($options['type'] == 'tags'){
         $html .= '<input class="form-control" type="tags" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
     	}elseif($options['type'] == 'prenom'){
         $html .= '<input class="form-control" type="prenom" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
     	}elseif($options['type'] == 'nom'){
         $html .= '<input class="form-control" type="nom" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
     	}elseif($options['type'] == 'date'){
         $html .= '<input class="form-control" type="date" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>' ;
     	}elseif($options['type'] == 'email'){
         $html .= '<input placeholder="xyz@example.com" class="form-control" required pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$" type="email" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
		}elseif($options['type'] == 'ville'){
         $html .= '<input class="form-control" type="ville" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
		}elseif($options['type'] == 'sexe'){
         $html .= '<input class="form-control" type="sexe" id="'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';
		}if($error){
			$html .= '<span class="help-inline">'.$error.'</span>';
		}
		$html .= '</div></div>';
		return $html;
	}

}