function insertTag(startTag, endTag, textareaId, tagType) {
        var field  = document.getElementById(textareaId);
        var scroll = field.scrollTop;
        field.focus();

        /* === Partie 1 : on récupère la sélection === */
        if (window.ActiveXObject) {
                var textRange = document.selection.createRange();
                var currentSelection = textRange.text;
        } else {
                var startSelection   = field.value.substring(0, field.selectionStart);
                var currentSelection = field.value.substring(field.selectionStart, field.selectionEnd);
                var endSelection     = field.value.substring(field.selectionEnd);
        }

        /* === Partie 2 : on analyse le tagType === */
        if (tagType) {
                switch (tagType) {
                        case "lien":
                        	endTag = "</lien>";
        					if (currentSelection) { // Il y a une sélection
                				if (currentSelection.indexOf("http://") == 0 || currentSelection.indexOf("https://") == 0 || currentSelection.indexOf("ftp://") == 0 || currentSelection.indexOf("www.") == 0) {
                        		// La sélection semble être un lien. On demande alors le libellé
                        			var label = prompt("Quel est le libellé du lien ?") || "";
                        			startTag = "<lien url=\"" + currentSelection + "\">";
                        			currentSelection = label;
                			} else {
                        		// La sélection n'est pas un lien, donc c'est le libelle. On demande alors l'URL
                        		var URL = prompt("Quelle est l'url ?");
                        		startTag = "<lien url=\"" + URL + "\">";
                			}
        					} else { // Pas de sélection, donc on demande l'URL et le libelle
                				var URL = prompt("Quelle est l'url ?") || "";
                				var label = prompt("Quel est le libellé du lien ?") || "";
                				startTag = "<lien url=\"" + URL + "\">";
                				currentSelection = label;
        					}
                        break;
                }
        }

        /* === Partie 3 : on insère le tout === */
        if (window.ActiveXObject) {
                textRange.text = startTag + currentSelection + endTag;
                textRange.moveStart("character", -endTag.length - currentSelection.length);
                textRange.moveEnd("character", -endTag.length);
                textRange.select();
        } else {
                field.value = startSelection + startTag + currentSelection + endTag + endSelection;
                field.focus();m
                field.setSelectionRange(startSelection.length + startTag.length, startSelection.length + startTag.length + currentSelection.length);
        }

        field.scrollTop = scroll;
}

function upload_image(){
        window.open("http://localhost/PHP_my_meetic/Blog/cockpit/medias/ajout",'upload_image','menubar=no, scrollbars=no, top=100, left=100, width=700, height=700');
}

function sexeChange(){// choix du sexe, inscription{
    var sexe = document.getElementById('sexe').alt;
    console.log(sexe);
    if(sexe == "homme")
    {
        document.getElementById('sexe').src = "/PHP_my_meetic/Blog/img/femme.png";
        document.getElementById('sexe').alt = "femme";
        document.getElementById('valuesexe').value = 1;
    }
    else if(sexe == "femme")
    {
        document.getElementById('sexe').src = "/PHP_my_meetic/Blog/img/homme.png";
        document.getElementById('sexe').alt = "homme";
        document.getElementById('valuesexe').value = 0;
    }
}

function deleteHobbie(id_hobbie, value)
{
    var content = document.getElementById('hobbies').value;
    document.getElementById(id_hobbie).style.display = "none";
    document.getElementById('hobbies').value = content.replace(value+";","");
}

function addHobbie()
{
    var id = document.getElementById('hobbie-val').className;
    var content = document.getElementById('hobbie-val').value;
    if(content.length > 1 && content.length < 40)
    {
        document.getElementById('error-hobbie').innerHTML = "";
        id = id.replace("hobbie","");
        var div= document.getElementById('div-hobbies');
        var newcontent = "<span class=\"hobbies\" id=\"hobbie"+id+"\">"+content+"<i class=\"glyphicon glyphicon-remove close\" onClick=\"deleteHobbie('hobbie"+id+"','"+content+"')\"></i></span>";
        document.getElementById('div-hobbies').innerHTML = div.innerHTML+newcontent;
        var value = document.getElementById('hobbies').value;
        document.getElementById('hobbies').value = value+content+";";
        document.getElementById('hobbie-val').value = "";
        idplus = (parseFloat(id) + 1 );
        document.getElementById('hobbie-val').className ="hobbie"+idplus;
    }
    else
    {
        document.getElementById('error-hobbie').innerHTML = "Taille du texte incorrect (compris entre 2 et 40 caractères)";
    }
}

/* INSCRIPTION / CONNEXION */
$(function(){
    $("#fieldsets").hide();
    $("#registerh2").click(function() {
        $("#fieldsets2").hide("slow");
        $("#fieldsets").show("slow");
    });
    $("#loginh2").click(function(){
        $("#fieldsets").hide("slow");
        $("#fieldsets2").show("slow");
    });
});

/*SEND MSG ON PROFIL*/
$(function(){
    $("#form-comment").hide();
    $("#comment-h2").click(function() {
        $("#form-comment").slideToggle("slow");
    });
});

/* CLOSE ALERTES */
$(function(){
 $(".closealert").click(function() {
        $("#alertmodal").hide("normal");
    });
});

/*
*USERS INDEX
*/

/*OPACITY*/
$(function(){
    $(".listusers .user-index").hover(function(){
        $(this).addClass("hoverprofil");
        $(".user-index").not(".hoverprofil").css("opacity","0.6");
    },function(){
        $(this).removeClass("hoverprofil");
        $(".user-index").css("opacity","1");
    });
});

$(function(){
    $(".hiddenprofil").hide();
});

/*RECHERCHE*/
$(function(){
    $('#dropsearch').click(function (){
        $('.searchinputs').slideToggle();
    });
});

/* MON COMPTE */
$(function(){
    $('.msglarge').click(function (){
        $('.menuaccount').slideToggle();
        // return false;
    });
});
/* NAVBAR UPLOAD */
$(function(){
    $('.msglarge').click(function (){
        $('.menumedias').slideToggle();
        // return false;
    });
});
/*-------UPLOAD IMAGE ----------*/
$(function(){
    // Clear event
    $('.image-preview-clear').click(function(){
        //$('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    $(".image-preview-input input:file").change(function (){
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
        }
        reader.readAsDataURL(file);
    });
});

/*SLIDETOGGLE DEBUG*/
$(function(){
    $('.debugs').click(function (){
        $('.varvar').slideToggle();
        return false;
    });
});

/*SLIDETOGGLE CONTENT MSG */
$(function(){
    $('.recus').slideToggle('slow');
});

/*MENU MESSAGERIE DROPDOWN*/
$(function(){
    $('.dropmsg').click(function (){
        $('.menumsg').slideToggle();
    });
});