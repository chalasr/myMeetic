function valid(f)
{
	if(!bonnom())
		return false;
	if(!bonnom1())
		return false;
	if(!bonmail())
		return false;
	if(!verifmail())
		return false;
	if(!mailverif(f))
		return false;
	if(!validation(f))
		return false;
	if(!bonmdp())
		return false;
	if(!showRadio())
		return false;
	
}

function bonnom()
{
	var reg = new RegExp(/^[a-zA-Z \-]+$/);
	var nom = document.getElementById('nom').value;
	var prenom = document.getElementById('prenom').value;
	var help = document.getElementById('help1');
	if (!nom.match(reg) || !prenom.match(reg))
	{
		help.innerHTML = "Nom ou Prénom incorrect";
		return false;
	}
		help.innerHTML = "";
		return(true);
}

function bonnom1(){

	var help = document.getElementById('help1');
	if(document.getElementById('nom').value.length > 2 && document.getElementById('nom').value.length < 16 && document.getElementById('prenom').value.length > 2 && document.getElementById('prenom').value.length < 16)
	{
		help.innerHTML = "";
		return true;
	}
	else
	{
		help.innerHTML = "De 3 à 15 caractères";
		return false;
	}

}

function bonmail()
{
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	var email = document.getElementById('email').value;
	var help = document.getElementById('help2');
	if (!email.match(reg))
	{
		help.innerHTML = "Erreur dans l'email";
		return false;
	}
	help.innerHTML = "";
	return(true);
}
function verifmail()

{
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	var email = document.getElementById('edd-emai-confirml').value;
	var help = document.getElementById('help3');
	if (!email.match(reg))
	{
		help.innerHTML = "Erreur dans l'email";
		return false;
	}
		help.innerHTML = "";
		return(true);
}

function validation(f) {

var createpassword = document.getElementById('createpassword');
var verifpassword = document.getElementById('verifpassword');
var help = document.getElementById('help4');


  if (f.createpassword.value == '' || f.verifpassword.value == '') {
	help.innerHTML = "Veuillez saisir un mot de passe";
	f.createpassword.focus();
	return false;
	}
  else if (f.createpassword.value != f.verifpassword.value) {
	help.innerHTML = "Les mots de passes ne sont pas identiques";
	f.createpassword.focus();
	return false;
	}
  else if (f.createpassword.value == f.verifpassword.value) {
	help.innerHTML = "";
	return true;
	}
  else {
	f.createpassword.focus();
	return false;
	}
  }

  function bonmdp(){

	var help = document.getElementById('help4');
	if(document.getElementById('createpassword').value.length > 3 && document.getElementById('createpassword').value.length < 101 && document.getElementById('verifpassword').value.length > 3 && document.getElementById('verifpassword').value.length < 101)
	{
		help.innerHTML = "";
		return true;
	}
	else
	{
		help.innerHTML = "Minimum 4 caractères";
		return false;
	}

}

  function mailverif(f) {

var help = document.getElementById('help2');

  if (f.user_email.value != f.verifemail.value) {
   help.innerHTML = "Les email ne sont pas identiques";
	f.user_email.focus();
	return false;
	}
  else if (f.user_email.value == f.verifemail.value) {
	help.innerHTML = "";
	return true;
	}
  else {
	f.user_email.focus();
	return false;
	}
  }

function showRadio() {
	var help = document.getElementById('help6');
	if(document.getElementById('femme').checked || document.getElementById('homme').checked){
		help.innerHTML = "";
		return true;	
	}
	else {
		help.innerHTML = "Veuillez sélectionner un sexe";
		return false;
	}
}