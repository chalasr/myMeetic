<?php
class Conf{

	static $debug = 1;

	static $databases = array(

			'default' => array(
			'host'		=> 'localhost',
			'database'	=> 'meetic',
			'login'		=> 'root',
			'password'  => 'root'
		)
	);
}

Router::prefix('cockpit','logged');


Router::connect('','pages/home');
Router::connect('cockpit','cockpit/users/index');
Router::connect('page/:slug-:id','pages/view/id:([0-9]+)/slug:([a-z0-9\-]+)');