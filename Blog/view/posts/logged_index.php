<div class="cadre-page">

	<div class="mytitle page-header">
		<h1>My Meetic</h1>
	</div>

	<?php if(!array_key_exists('search',$_POST)){ ?>
	<div class="row">
			<?php foreach($posts as $k => $v): ?>
			<div class="thumbnail post-taille">
					<h2> <?php echo $v->name; ?></h2>
					<div class="index-article">
						<?php echo $v->content;?>
					</div>
					<p class="articles"><a href="<?php echo Router::url("posts/view/id:{$v->id}/slug:$v->slug");?>">Voir l'article &rarr;</a><p>
			</div>
		<?php endforeach ?>
	</div>

	<div>
		<ul class="pagination perpage">
			<li><a href="?page=1">&laquo;</a></li>
			<?php for($i=1; $i <= $page; $i++): ?>
			<li <?php if($i==$this->request->page) echo 'class="active"';
			?>><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
			  <?php endfor; ?>
		</ul>
	</div>
	<?php }elseif($_POST['search']) {
		$this->Post->search();
	}elseif($_POST['search'] === ""){
		$this->redirect('cockpit/posts/index');
	} ?>
</div>