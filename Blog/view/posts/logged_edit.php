<div class="page-header">
	<h1>Editer un article </h1>
</div>

<form action="<?php echo Router::url('logged/posts/edit/'.$id); ?>" method="post" class="form-horizontal">
	<?php echo $this->Form->input('name','Titre'); ?>
	<?php echo $this->Form->input('slug','Url'); ?>
	<?php echo $this->Form->input('tags','Tags'); ?>
	<?php echo $this->Form->input('id','hidden'); ?>
	<div>
	<?php echo $this->Form->input('content','Contenu',array(
		'type'=>'textarea','class'=>'form-control','rows'=>5)); ?>
	</div>
	<?php echo $this->Form->input('online','En ligne',array(
		'type'=>'checkbox')); ?>

	<div class="form-actions">
		<input type="submit" class="btn btn-primary" value="Envoyer">
	</div>
</form>
