<div class="mytitle page-header">
	<h1>Gestion des articles</h1>
</div>

<div class="page-heading">
	<h3><?php echo $total; ?> Articles</h3>
</div>

<div class="panel panel-default">
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>En ligne ?</th>
			<th>Titre</th>
			<th>Tags</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($posts as $k => $v): ?>
		<tr>
				<td><?php echo $v->id; ?></td>
				<td><span class="label <?php echo ($v->online==1)?'label label-success':'label label-default'; ?>"><?php echo ($v->online==1)?'En ligne':'Hors ligne'; ?></span></td>
				<td class="bleu"><?php echo $v->name; ?></td>
				<td> <?php echo $v->tags; ?></td>
				<td>
					<a class="btn btn-default btn-xs" href="<?php echo Router::url('logged/posts/edit/'.$v->id); ?>">Editer</a>&nbsp;
					<a class="btn btn-default btn-xs" onclick="return confirm('Voulez vous vraiment supprimer ce contenu'); " href="<?php echo Router::url('logged/posts/delete/'.$v->id); ?>">Supprimer</a>
				</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
</div>

<a href="<?php echo Router::url('logged/posts/edit'); ?>" class="btn btn-primary btn-lg">Ajouter un article</a>