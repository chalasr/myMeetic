<div class="row">
    <div class="well article-complet">
        <?php $title_for_layout = $post->name; ?>
        <div class="article-header">
            <h1><?php echo $post->name; ?></h1>
            <p>Article publié le :&nbsp;<?php echo $post->created; ?></p>
            <hr>
        </div>
        <div class="contenu">
            <?php echo $post->content; ?>
        </div>
        <p class="tags">tags :<?php echo $post->tags;?></p>
    </div>
</div>

<div class="commentaires">
    <a class="btn btn-primary btn-lg" id="comment-h2" href="#comment-h2">Ajouter un commentaire &nbsp;&nbsp;<span class="glyphicon glyphicon-plus"></span></a>
    <div id="ajout-commentaire">
        <form id="form-comment" action="#" method="post">
            <p id="closeajout"><a href="#comment-h2" id="delete" class="glyphicon glyphicon-remove"></a></p>
            <label>Login :</label>
            <br/>
            <input type="text" name="username" placeholder="Votre login" <?php if($this->Session->isLogged()) echo 'value="'.$_SESSION['User']->login.'"'; ?> >
            <br/><label>Commentaire :</label>
            <br/>
            <input type="hidden" name="parent_id" value="0">
            <textarea placeholder="Votre commentaire ici" name="content" class="form-control" rows=5></textarea>
            <div class="form-actions">
                <input type="submit" class="btn btn-primary marge" value="Envoyer">
            </div>
        </form>
    </div>

    <?php foreach ($comments as $value) : ?>
        <div class="well comment-cadre">
                <div class="comment row">
                    <span class="comment-by">Commentaire écrit par :&nbsp;</span><?php echo $value->username; ?>
                    <?php if($this->Session->isLogged()) echo '<span><a href="'.Router::url('posts/deletecomment/'.$value->id_comment).'"class="glyphicon glyphicon-remove delete"></a></span>' ?>
                    <div class="comment-content">
                        <?php echo $value->content; ?>
                    </div>
                    <div class="mic-info">
                        <?php echo $value->created; ?>
                    </div>
                </div>
        </div>
    <?php endforeach ?>
</div>
