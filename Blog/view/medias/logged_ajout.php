<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;Photo de Profil <i class="icondown btn btn-default glyphicon glyphicon-picture"></i></span>
            <div class="menuaccount">
                <a href="<?php echo Router::url('cockpit/medias/view'); ?>" class="list-group-item profmenu">Avatar&nbsp;&nbsp;<i class="iconprof2 glyphicon glyphicon-picture"></i></a>
                <a href="#" class="list-group-item profmenu"> Informations&nbsp;<i class="iconprof4 glyphicon glyphicon-cog"></i> </a>
                <a href="<?php echo Router::url('cockpit/messages/msgrecus'); ?>" class="list-group-item profmenu">
                    Messagerie <span class="iconprof5 glyphicon glyphicon-envelope"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<form class="formmedia" action="<?php echo Router::url('logged/medias/ajout'); ?>" method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="precedentajout">
    		<a href="javascript:history.go(-1)" class="editbtn btn btn-default btn-lg glyphicon glyphicon-arrow-left"></a>
		</div>
		<div class="uploadav col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="form-group">
				<input required placeholder="Titre de ton image" class="form-control" type="text" name="name" >
			</div>
			<div class="input-group image-preview">
			  	<input required placeholder="Choisis une image en cliquant sur 'Browse' " type="text" class="form-control image-preview-filename">
			  	<div class="input-group-btn">
			        <button type="button" class="previewcl btn btn-default image-preview-clear">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/>
                    </div>
				</div>
			</div>
			<div class="actions">
				<input type="submit" value="Définir comme photo de profil" class="btn btn-gay marge">
			</div>
		</div>
	</div>
</form>
<hr>