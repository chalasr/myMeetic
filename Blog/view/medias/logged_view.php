<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;Photo de Profil <i class="icondown btn btn-default glyphicon glyphicon-arrow-down"></i></span>
            <div class="menumedias">
                <a href="<?php echo Router::url('cockpit/users/myaccount'); ?>" class="list-group-item profmenu">
                    <?php echo $_SESSION['User']->prenom." ".$_SESSION['User']->nom; ?>
                    <i class="iconprof glyphicon glyphicon-user"></i>
                </a>
                <a href="<?php echo Router::url('cockpit/messages/msgrecus'); ?>" class="list-group-item profmenu">
                    <i class=""></i> Messagerie <span class="iconprof5 glyphicon glyphicon-envelope"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="accountview article-complet">
        <div class="article-header">
            <h2>Ma photo de profil</h2>
            <hr>
        </div>
        <div class="contenu-medias">
        <?php foreach ($medias as $value) : ?>
            <div class="medias-index">
                <img src="<?php echo Router::webroot('webroot/img/upload/'.$value->file); ?>" alt="image" />
            </div>
            <?php endforeach; ?>
        </div>
        <div class="changediv">
            <a class="change btn btn-gay" href="<?php echo Router::url('cockpit/medias/ajout'); ?>">Changer ma photo de profil</a>
        </div>
    </div>
</div>