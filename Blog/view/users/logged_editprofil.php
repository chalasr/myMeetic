<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><i class="glyphicon glyphicon-wrench"></i>&nbsp;&nbsp;Mettre à jour mes informations  <a href="javascript:history.go(-1)" class="return btn btn-default glyphicon glyphicon-arrow-left"></a></span>
            <a href="<?php echo Router::url('cockpit/medias/view'); ?>" class="list-group-item profmenu">Avatar&nbsp;&nbsp;<i class="iconprof2 glyphicon glyphicon-picture"></i></a>
            <a href="<?php echo Router::url('cockpit/messages/msgrecus'); ?>" class="list-group-item profmenu">
                Messagerie <span class="iconprof5 glyphicon glyphicon-envelope"></span>
            </a>
        </div>
    </div>
</div>

<form action="<?php echo Router::url('cockpit/users/editprofil/'); ?>" method="post" class="formedit form-horizontal">
	<?php echo $this->Form->input('password','Nouveau Mot de passe',array('type'=>'password')); ?>
    <?php echo $this->Form->input('email', 'Nouvelle adresse e-mail', array('type' => 'email')); ?>

	<div class="form-actions">
		<input type="submit" class="btn btn-primary marge" value="Envoyer">
	</div>
</form>
