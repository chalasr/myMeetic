<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><i class="glyphicon glyphicon-heart"></i>&nbsp;&nbsp;Rencontres <i class="icondown btn btn-default glyphicon glyphicon-arrow-up"></i></span>
            <div class="menumedias">
                <a id="comment-h2" href="#" class="list-group-item profmenu">
                    Envoie lui un message <span class="iconprof glyphicon glyphicon-pencil"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="profilview article-complet">

        <div class="article-header">
            <h2>
                <?php
                    echo $user->pseudo;
                    if($user->sexe == 1){
                        echo '<img class="genre" src="/PHP_my_meetic/Blog/img/femme2.png" alt="femme"/>';
                    }else{
                        echo '<img class="genre" src="/PHP_my_meetic/Blog/img/homme2.png" alt="femme"/>';
                    }
                ?>
            </h2>
            <hr>
        </div>
        <div class="contenu">
            <div class="avatar">
                <?php echo $avatar; ?>
            </div>
            <p class="fullname">
                <?php echo $user->prenom; ?> <br>
                <?php echo $this->User->age($user->naissance)." ans"; ?> <br>
                Habite à <?php echo $user->ville.' ('.$user->region; ?>)
            </p>
        </div>
        <div class="commentaires">
            <div id="ajout-commentaire">
                <form id="form-comment" action="<?php echo Router::url('cockpit/messages/envoimsg'); ?>" method="post">
                    <input type="hidden" name="sendername" value="<?php echo $_SESSION['User']->pseudo; ?>" >
                    <input type="hidden" name="username" value="<?php echo $user->pseudo; ?>">
                    <br/>
                    <textarea required placeholder="Votre message ici" name="content" class="form-control textamsg" rows=5></textarea>
                    <div class="form-actions">
                        <input type="submit" class="send btn btn-gay marge" value="Envoyer">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>