<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <form action="<?php echo Router::url('users/inscription'); ?>" method="post" class="inscription form-horizontal">
            <fieldset id="fieldsets">
                <div class="pseudo">
                    <?php echo $this->Form->input('pseudo','Pseudonyme'); ?>
                </div>
                <?php echo $this->Form->input('password','Mot de passe',array('type'=>'password')); ?>
                <?php echo $this->Form->input('nom','Nom'); ?>
                <?php echo $this->Form->input('prenom','Prénom'); ?>
                <?php echo $this->Form->input('naissance','Date de naissance', array('type'=>'date')); ?>
                <?php echo $this->Form->input('email','Adresse e-mail', array('type'=>"email")); ?>
                <?php echo $this->Form->input('pays','Pays'); ?>
                <?php echo $this->Form->input('region','Région'); ?>
                <?php echo $this->Form->input('departement','Département'); ?>
                <?php echo $this->Form->input('ville','Votre ville'); ?>
                <label class="col-sm-5">Sexe</label>
                <div class="sexe form-group">
                    <img id="sexe" src="/PHP_my_meetic/Blog/img/<?php
                    if(isset($_POST['sexe']) && $_POST['sexe'] == "1"){ echo "femme.png";
                    }else { echo "homme.png"; }?>" alt="homme" onClick="sexeChange();" >
                </div>
                <input type="hidden" name="sexe" id="valuesexe" value="0">
                <div class="form-actions">
                    <input id="sinscrire" type="submit" class="btn btn-gay marge" value="S'inscrire">
                </div>
            </fieldset>
        </form>
    </div>
</div>