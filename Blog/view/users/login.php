<div class="page-header mytitle">
	<h1>Bienvenue sur My Meetic</h1>
</div>
<div class="signin">
	<form action="<?php echo Router::url('users/login'); ?>" method="post" class="form-horizontal">
		<div>
		<fieldset id="fieldsets2">
			<div class="connexion-inputs">
				<?php echo $this->Form->input('pseudo','Identifiant'); ?>
				<?php echo $this->Form->input('password','Mot de passe',array('type'=>'password')); ?>
			</div>
			<div class="marge">
				<input id="submitco" type="submit" class="btn btn-gay" value="Se connecter">
			</div>
		</fieldset>
		<?php echo "<p id=\"connect\"><a id=\"loginh2\" href=\"#loginh2\"> Connectez-vous </a> à l'aide de vos identifiants ou <a id=\"registerh2\" href=\"#registerh2\">Inscrivez-vous</a></p>";?>
		</div>
	</form>
</div>