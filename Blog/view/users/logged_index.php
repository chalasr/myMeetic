<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><i class="glyphicon glyphicon-heart"></i>&nbsp;&nbsp;Rencontres</span>
        </div>
    </div>
</div>
<div class="cadre-page">
	<div class="searchform">
		<a id="dropsearch" class="btn btn-default btn-lg" href="#">Recherche</a>
		<form class="navbar-form searchinputs" style="display:none;" method="post" action="#" role="search">
			<div class="col-sm-12">
	            <label for="sexe">Sexe&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	            <select name="sexe" id="sexe">
	            	<option id="3" value="3">Tous</option>
	  				<option id="0" value="2">Homme</option>
	  				<option id="1" value="1">Femme</option>
	  			</select>
	  			<br><br>
            </div>
            <div class="col-sm-10">
	          	<label for="searchage">Âge &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	          		<select name="tranche" id="searchage">
	          			<option value="5">Tous</option>
	          			<option value="1">18 - 25</option>
	          			<option value="2">25 - 35</option>
	          			<option value="3">35 - 45</option>
	          			<option value="4">+ de 45</option>
	          		</select>
	          		<br><br>
	        </div>
	        <div class="col-sm-12">
		        <label>Pays &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<select name="pays" class="selectpays" >
					<option value="tous">Tous</option>
					<option value="France">France</option>
					<option value="Autres">Autres</option>
				</select>
				<br><br>
			</div>
			<div class="col-sm-12">
		        <label>Région &nbsp;</label>
				<select name="region" class="selectpays" >
					<option value="toutes">Toutes</option>
					<?php foreach($region as $v): ?>
					<?php echo '<option value="'.$v->region.'">'.$v->region.'</option>'; ?>
					<?php endforeach; ?>
				</select>
				<br><br>
			</div>
			<div class="col-sm-12">
		        <label>Départ<br>-ement &nbsp;</label>
				<select name="departement" class="selectpays" >
					<option value="toutes">Tous</option>
					<?php foreach($departement as $v): ?>
					<?php echo '<option value="'.$v->departement.'">'.$v->departement.'</option>'; ?>
					<?php endforeach; ?>
				</select>
				<br><br>
			</div>
	        <div class="col-sm-12">
		        <label>Ville(s) &nbsp;</label>
				<select id="hobbie-val" name="choix" class="hobbie input-add" >
					<option value="toutes">Toutes</option>
					<?php foreach($ville as $v): ?>
					<?php echo '<option value="'.$v->ville.'">'.$v->ville.'</option>'; ?>
					<?php endforeach; ?>
				</select>
				<i class="glyphicon glyphicon-plus btn btn-default btn-sm" id="villeplus" onClick="addHobbie()"></i>
				<span id="error-hobbie"></span>
				<input type="hidden" name="ville" id="hobbies" value="">
				<span class="div-hobbies" id="div-hobbies">
				</span>
				<button id="subsearch" class="btn btn-default btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
	   	</form>
	</div>
	<?php if(!isset($_POST['sexe'])){ ?>
	<div class="row listusers">
		<div class="nbrusers">
			<h2> <?php echo $total; ?> membres</h2>
		</div>
		<?php foreach($users as $k => $v): ?>
		<?php if(($v->id == $_SESSION['User']->id)) continue; ?>
		<div class="user-index">
			<h2> <?php echo $v->pseudo; ?></h2>
			<div class="index-article">
				<a href="<?php echo Router::url("cockpit/users/view/$v->id");?>"> <?php echo $v->avatars; ?> </a>
				<p><?php echo $this->User->age($v->naissance)." ans"; ?></p>
			</div>
			<p class="viewprofil"><a href="<?php echo Router::url("cockpit/users/view/$v->id");?>">Voir le profil &rarr;</a><p>
		</div>
		<?php endforeach ?>
	</div>
	<div>
		<ul class="pagination perpage">
			<li><a href="?page=1">&laquo;</a></li>
			<?php for($i=1; $i <= $page; $i++): ?>
			<li <?php if($i==$this->request->page) echo 'class="active"';
			?>><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
			<?php endfor; ?>
		</ul>
	</div>
	<?php
		}elseif(!empty($_POST['sexe'])){
			$this->User->search();
		}
	?>
</div>