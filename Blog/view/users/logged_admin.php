<div class="mytitle page-header">
<h1>Gestion des utilisateurs</h1>
</div>

<div class="page-heading">
	<h3><?php echo $total ?> utilisateurs</h3>
</div>

<div class="panel panel-default">
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Login</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $k => $v): ?>
		<tr>
				<td><?php echo $v->id; ?></td>

				<td class="bleu"><?php echo $v->pseudo; ?></td>
				<td>
					<a class="btn btn-default btn-xs" href="<?php echo Router::url('users/editprofil/'.$v->id); ?>" >Editer</a> &nbsp;
					<a class="btn btn-default btn-xs" onclick="return confirm('Voulez vous vraiment supprimer ce contenu');" href="<?php echo Router::url('logged/users/delete/'.$v->id); ?>" >Supprimer</a>
				</td>
		</tr>
		<?php endforeach ?>

	</tbody>
</table>
</div>

<a href="<?php echo Router::url('logged/users/ajout'); ?>" class="btn btn-primary btn-lg">Ajouter un utilisateur</a>