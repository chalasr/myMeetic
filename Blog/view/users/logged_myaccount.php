    <div class="row">
        <div class="dropmsg col-sm-4 col-md-5 sidebar">
            <div class="list-group2">
                <span class="msglarge list-group-item active"><span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;Mon Profil <i class="icondown btn btn-default glyphicon glyphicon-wrench"></i></span>
                <div class="menuaccount">
                    <a href="<?php echo Router::url('cockpit/medias/view'); ?>" class="list-group-item profmenu">Avatar&nbsp;&nbsp;<i class="iconprof2 glyphicon glyphicon-picture"></i></a>
                    <a href="<?php echo Router::url('cockpit/users/editprofil'); ?>" class="list-group-item profmenu"> Informations&nbsp;<i class="iconprof4 glyphicon glyphicon-cog"></i> </a>
                    <a href="<?php echo Router::url('cockpit/messages/msgrecus'); ?>" class="list-group-item profmenu">
                        Messagerie <span class="iconprof5 glyphicon glyphicon-envelope"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="profilview article-complet">
            <div class="article-header">
                <h2><?php echo $_SESSION['User']->pseudo; ?></h2>
                <hr>
            </div>
            <div class="contenu">
                <div>
                    <a href="<?php echo Router::url('cockpit/medias/view'); ?>"><?php echo $avatar; ?></a>
                </div>
                <p class="fullname">
                    <?php echo $_SESSION['User']->prenom; ?>
                    <?php echo $_SESSION['User']->nom; ?> <br>
                    <?php echo $this->User->age($_SESSION['User']->naissance)." ans"; ?> <br>
                    Habite à <?php echo $_SESSION['User']->ville.' ('.$_SESSION['User']->region; ?>)
                </p>
            </div>
            <div class="delacc">
                <a href="<?php echo Router::url('users/desactiveUser'); ?>" class="send btn btn-gay" onclick="return confirm('Voulez-vous vraiment supprimer votre compte définitivement ?');">Supprimer mon compte définitivement</a>
            </div>
        </div>
    </div>
