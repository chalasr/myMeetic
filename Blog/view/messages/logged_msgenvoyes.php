<div class="row">
    <div class="dropmsg col-sm-4 col-md-5 sidebar">
        <div class="list-group2">
            <span class="msglarge list-group-item active"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;Messagerie <i class="icondown btn btn-default glyphicon glyphicon-arrow-down"></i></span>
            <div class="menumedias">
                <a href="<?php echo Router::url('cockpit/messages/msgrecus'); ?>" class="list-group-item profmenu">Messages Reçus</a>
                <a href="<?php echo Router::url('cockpit/messages/msgenvoyes'); ?>" class="list-group-item profmenu">Messages Envoyés</a>
            </div>
        </div>
    </div>
</div>

<div class="messagerie-index">
    <div>
        <h3>Boîte d'envoi&nbsp;&nbsp;<span class="btn btn-default nbrmsg badge"><?php echo $total; ?> </span></h3>
    </div>
    <div class="recus panel panel-default">
        <table class="table">
            <thead class="titremenu">
                <tr>
                    <th>Pseudo</th>
                    <th>Date</th>
                    <th>Message</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($messagerie as $k => $v) : ?>
                <tr>
                    <td><?php echo $v->username; ?></td>
                    <td><?php echo $v->created; ?></td>
                    <td ><?php echo $v->content; ?></td>
                    <td><a class="btn btn-default glyphicon glyphicon-trash" href="<?php echo Router::url('cockpit/messages/delete/'.$v->id); ?>"></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div>
    <ul class="pagination msg">
        <li><a href="?page=1">&laquo;</a></li>
        <?php for($i=1; $i <= $page; $i++): ?>
        <li <?php if($i==$this->request->page) echo 'class="active"';
        ?>><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>
</div>