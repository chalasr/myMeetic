<?php
    if(!$this->Session->isLogged()){
        $connexion = __dir__."/../users/login.php";
        $inscription = __dir__."/../users/inscription.php";
        // echo $connexion;
        // echo $inscription;
        include($connexion);
        include($inscription);
    }else{
        $this->redirect('cockpit/users/index');
    }
?>
