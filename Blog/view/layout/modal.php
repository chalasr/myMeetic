<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo isset($title_for_layout)?$title_for_layout:'Administration'; ?></title>
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/bootstrap.css">
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/style.css">
</head>
<body>

<div class="container">
    <div class="body-complete-modal">
        <?php echo $this->Session->flash(); ?>
        <?php echo $content_for_layout; ?>
    </div>
</div>

<script type="text/javascript" src="/PHP_my_meetic/Blog/js/jquery.js"></script>
<script type="text/javascript" src="/PHP_my_meetic/Blog/js/style.js"></script>
</body>
</html>