<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo isset($title_for_layout)?$title_for_layout:'My Meetic'; ?></title>
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/bootstrap.css">
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/style.css">
</head>
<body>

    <header class="nav sub-menu menu">
        <nav class="container">
            <ul class="navmenu list-unstyled">
                <li class="accueil"><a href="<?php echo Router::url('cockpit'); ?>"><strong>My Meetic</strong></a></li>
                <li><a href="<?php echo Router::url('cockpit/users/myaccount/'); ?>">Mon compte</a></li>
                <li><a href="<?php echo Router::url('logged/messages/msgrecus'); ?>">Messagerie</a></li>
                <?php $pagesMenu = $this->request('Pages', 'getMenu'); ?>
                <?php foreach($pagesMenu as $p): ?>
                <li><a href="<?php echo BASE_URL.'/pages/view/'.$p->id; ?>" title="<?php echo $p->name; ?>"><?php echo $p->name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </nav>
        <div><?php if($this->Session->isLogged()) echo '<p class="login-bouton info-user"><a href="'.Router::url('cockpit/users/myaccount').'"> <strong class="pseudout">&nbsp;'.$_SESSION['User']->pseudo.'</strong></a>&nbsp;<a class="btn btn-gay btn-sm" href="'.Router::url('users/logout').'">Déconnexion <i class="glyphicon glyphicon-off"></i></a></p>'; ?></div>
    </header>

<div class="container">
    <div class="body-complete-logged">
        <?php echo $this->Session->flash(); ?>
        <?php echo $content_for_layout; ?>
    </div>
    <footer>
        <p> My_meetic &nbsp;{chalas_r}&copy; </p>
    </footer>
</div>

<script type="text/javascript" src="/PHP_my_meetic/Blog/js/jquery.js"></script>
<script type="text/javascript" src="/PHP_my_meetic/Blog/js/style.js"></script>
</body>
</html>
