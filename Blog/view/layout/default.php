<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
  	<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo isset($title_for_layout)?$title_for_layout:'My Meetic'; ?></title>
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/bootstrap.css">
    <link rel="stylesheet" href="/PHP_my_meetic/Blog/css/style.css">
</head>
<body>
	<div class="container">
		<div class="body-complete">
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		</div>
		<footer>
			<p> My_meetic&copy; &nbsp;{chalas_r} </p>
		</footer>
	</div>
<script type="text/javascript" src="/PHP_my_meetic/Blog/js/jquery.js"></script>
<script type="text/javascript" src="/PHP_my_meetic/Blog/js/style.js"></script>
</body>
</html>