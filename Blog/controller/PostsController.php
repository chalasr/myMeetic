<?php
class PostsController extends Controller{

	function logged_index(){
		$perPage = 2;
		$this->loadModel('Post');
		$condition = array('online' => 1,'type'=>'post');
		$d['posts'] = $this->Post->find(array(
			'conditions' => $condition,
			'limit' => ($perPage*($this->request->page-1)).','.$perPage
				));
				$d['total'] = $this->Post->findCount($condition);
				$d['page'] = ceil($d['total'] / $perPage);
				$this->set($d);
	}


	function logged_view($id,$slug){
		$this->loadModel('Post');
		/*Ajout de commentaires*/
		$this->loadModel('Comment');
		if($_POST){
			$this->Comment->saveComment('post', $id, $_POST['username'], 'user@user.com');
		}
		$d['post'] = $this->Post->findFirst(array(
			'fields'	=> 'id,slug,content,name,created,tags',
			'conditions' => array('online' => 1,'id'=>$id,'type'=>'post')
		));

		$d['comments'] = $this->Comment->findAll('post',$id);
		if(empty($d['post'])){
			$this->e404('Page introuvable');
		}
		if($slug != $d['post']->slug){
			$this->redirect("posts/view/id:$id/slug:".$d['post']->slug,301);
		}
		$this->set($d);
	}

	function deletecomment($id){
		$this->loadModel('Comment');
		$this->Comment->delComment($id);
		$this->redirect('posts/index');
		$this->Session->setFlash('Le commentaire a bien été supprimé');
	}

	/**
	*ADMIN
	**/

	function 	logged_admin(){
		$perPage = 10;
		$this->loadModel('Post');
		$condition = array('type'=>'post');
		$d['posts'] = $this->Post->find(array(
			'fields' => 'id,name,online,tags',
			'conditions' => $condition,
			'limit' => ($perPage*($this->request->page-1)).','.$perPage
		));
		$d['total'] = $this->Post->findCount($condition);
		$d['page'] = ceil($d['total'] / $perPage);
		$this->set($d);
	}

	/**
	*Permet d'éditer un article
	**/
function logged_edit($id = null){
		$this->loadModel('Post');
		if($id === null){
			$post = $this->Post->findFirst(array(
				'conditions' => array('online' => -1),
			));
			if(!empty($post)){
				$id = $post->id;
			}else{
				$this->Post->save(array(
					'online' => -1,
					'created' => date('Y-m-d')
				));
				$id = $this->Post->id;
			}
		}
		$d['id'] = $id;
		if($this->request->data){
			if($this->Post->validates($this->request->data)){
				$this->request->data->type = 'post';

				$this->Post->save($this->request->data);
				$this->Session->setFlash('Le contenu a bien été modifié', 'alert alert-success');
				$this->redirect('logged/posts/admin');
			}else{
				$this->Session->setFlash('Merci de corriger vos informations','alert alert-danger');
			}

		}else{
			$this->request->data = $this->Post->findFirst(array(
				'conditions' => array('id'=>$id)
			));
		}
		$this->set($d);
	}

	/**
	*Permet de supprimer un article
	**/
	function logged_delete($id){
		$this->loadModel('Post');
		$this->Post->delete($id);
		$this->Session->setFlash('L\'article a bien été supprimé', 'alert alert-success');
		$this->redirect('logged/posts/admin');
	}


}

