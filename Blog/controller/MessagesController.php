<?php
class MessagesController extends Controller{

    function logged_messagerie(){

    }

    function logged_envoimsg(){
        $this->loadModel('Message');
        $d['id'] = '';
        $sendername = $_SESSION['User']->pseudo;
        $envoyeur_id = $_SESSION['User']->id;
        $this->Message->save(array(
                    'envoyeur_id' => $envoyeur_id,
                    'content' => $_POST['content'],
                    'username' => $_POST['username'],
                    'sendername' => $sendername,
                    'created' => date('Y-m-d H:i:s')
        ));
        $vue = substr($_SERVER["HTTP_REFERER"], 36);
        $this->Session->setFlash('Votre message a bien été envoyé !');
        $this->redirect($vue);
        $this->set($d);
    }

    //http://localhost/PHP_my_meetic/Blog/cockpit/users/view/
   // a garder : /cockpit/users/view/74

    function logged_msgenvoyes(){
        $perPage = 6;
        $this->loadModel('Message');
        $my_id = $_SESSION['User']->id;
        $condition = array('envoyeur_id' => $my_id);
        $d['messagerie'] = $this->Message->find(array(
            'fields' => 'id,username,envoyeur_id,sendername,content,created',
            'order'  => 'id DESC',
            'conditions' => $condition,
            'limit' => ($perPage*($this->request->page-1)).','.$perPage
        ));
        $d['total'] = $this->Message->findCount($condition);
        $d['page'] = ceil($d['total'] / $perPage);
        $this->set($d);
    }

    function logged_msgrecus(){
        $perPage = 6;
        $this->loadModel('Message');
        $my_name = $_SESSION['User']->pseudo;
        $condition = array('username' => $my_name);
        $d['messagerie'] = $this->Message->find(array(
            'fields' => 'id,username,envoyeur_id,sendername,content,created',
            'conditions' => $condition,
            'limit' => ($perPage*($this->request->page-1)).','.$perPage
        ));
        $d['total'] = $this->Message->findCount($condition);
        $d['page'] = ceil($d['total'] / $perPage);
        $this->set($d);
    }

    function logged_delete($id){
        $this->loadModel('Message');
        $this->Message->delete($id);
        $this->Session->setFlash('Le message a bien été supprimé','alert alert-success');
        if($_SERVER["HTTP_REFERER"] == 'http://localhost/PHP_my_meetic/Blog/cockpit/messages/msgrecus'){ // Redirige vers la page précédente !
            $this->redirect('cockpit/messages/msgrecus');
        }else{
            $this->redirect('cockpit/messages/msgenvoyes');
        }
    }

}