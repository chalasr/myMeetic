<?php
class UsersController extends Controller{
/**
*INSCRIPTION
**/
	function inscription($id = NULL){
		$this->loadModel('User');
		$d['id'] = '';
		if($this->request->data){
			if($this->User->validates($this->request->data)){
				$this->request->data->actif = '0';
				$data = $this->request->data;
				$data->password = sha1($data->password);
				if($this->User->age($data->naissance) <= 25){
					$data->tranche = 1;
				}elseif($this->User->age($data->naissance) >= 25 && $this->User->age($data->naissance) <= 35){
					$data->tranche = 2;
				}elseif($this->User->age($data->naissance) >= 35 && $this->User->age($data->naissance) <= 45){
					$data->tranche = 3;
				}else{
					$data->tranche = 4;
				}
				if($this->User->age($data->naissance) >= 18){
					if(!$this->User->checkPseudo($data->pseudo)){
						if(!$this->User->checkEmail($data->email)){
							$this->User->save($this->request->data);
							$this->Session->setFlash('Vous etes désormais inscris ! <br>Un e-mail vous a été envoyé afin d\'activer votre compte <br> <a href="javascript:history.go(-1)">Connectez-vous une fois votre compte actif','alert alert-success');
						}else{
							$this->Session->setFlash('Cet e-mail est déjà prise !<a class="retourform" href="javascript:history.go(-1)">Retour au formulaire</a>', 'alert alert-danger');
						}
					}else{
						$this->Session->setFlash('Ce pseudo est déjà pris !<a class="retourform" href="javascript:history.go(-1)">Retour au formulaire</a>', 'alert alert-danger');
					}
				}else{
					$this->Session->setFlash('Vous devez avoir + de 18 ans pour vous inscrire !', 'alert alert-danger');
				}
			}else{
				$this->Session->setFlash('Merci de corriger vos informations','alert alert-danger');
			}
		}
		$this->set($d);
	}
	/**
	* Login
	**/
	function login(){
		if($this->request->data){
			$data = $this->request->data;
			$data->password = sha1($data->password);
			$this->loadModel('User');
			$user = $this->User->findFirst(array(
				'conditions' => array('pseudo' => $data->pseudo,'password' => $data->password
			)));
			if(!empty($user) && $user->actif != 1){
				$this->Session->write('User',$user);
			}else{
				$this->Session->setFlash('Login ou Mot de passe incorrect !', 'alert alert-danger');
			}
			$this->request->data->password = '';
		}
		if($this->Session->isLogged()){
				$this->redirect('cockpit/users/index');
		}
	}

	/**
	* Logout
	**/
	function logout(){
		unset($_SESSION['User']);
		$this->Session->setFlash('Vous êtes maintenant déconnecté');
		$this->redirect('');
	}

	function logged_index(){
		$perPage = 4;
		$this->loadModel('User');
		$condition = array('role');
		$d['users'] = $this->User->find(array(
			'fields' => 'id,pseudo,prenom,sexe,naissance,actif,ville,region,departement,naissance,tranche',
			'order' => 'id DESC',
			'limit' => ($perPage*($this->request->page-1)).','.$perPage
		));
		foreach ($d['users'] as $key => $value) {
			$d['users'][$key]->avatars= $this->logged_loadAvatar($value->id);
		}
		$d['total'] = $this->User->findCount($condition);
		$d['page'] = ceil($d['total'] / $perPage);
		$e['ville'] = $this->User->find(array(
			'fields' => 'ville',
			'distinct' => 'DISTINCT'
			));
		$e['region'] = $this->User->find(array(
			'fields' => 'region',
			'distinct' => 'DISTINCT'
			));
		$e['departement'] = $this->User->find(array(
			'fields' => 'departement',
			'distinct' => 'DISTINCT'
			));
		$this->set($e);
		$this->set($d);
	}

	function logged_view($id){
		$this->loadModel('User');
		$d['user'] = $this->User->findFirst(array(
			'fields' => 'id,pseudo,prenom,sexe,ville,region,departement,naissance',
			'conditions' => array('id'=>$id)
		));
		$d['avatar'] = $this->logged_loadAvatar($d['user']->id);
		if(empty($d['user'])){
			$this->e404('L\'utilisateur n\'existe plus');
		}
		$this->set($d);
	}

		function logged_admin(){
		$perPage = 10;
		$this->loadModel('User');
		$condition = array('role');
		$d['users'] = $this->User->find(array(
			'fields' => 'id,pseudo,nom',
			'limit' => ($perPage*($this->request->page-1)).','.$perPage
		));
		$d['total'] = $this->User->findCount($condition);
		$d['page'] = ceil($d['total'] / $perPage);
		$this->set($d);
	}

	function logged_delete($id){
		$this->loadModel('User');
		$this->User->delete($id);
		$this->Session->setFlash('L\'utilisateur a bien été supprimé','alert alert-success');
		$this->redirect('logged/users/index');
	}

	function 	logged_myaccount(){
		$this->loadModel('User');
		$my_id = $_SESSION['User']->id;
		$d['user'] = $this->User->findFirst(array(
				'conditions' => array('id'=>$my_id)
			));
		$d['avatar'] = $this->logged_loadAvatar($d['user']->id);
		$this->set($d);
	}

	function 	desactiveUser(){
		$this->loadModel('User');
		$this->User->desactive($_SESSION['User']->id);
		$this->logout();
	}

	function logged_editprofil($id = null){
		$this->loadModel('User');
		if($id === null){
			$user = $this->User->findFirst(array(
				'conditions' => array('id' => $_SESSION['User']->id)
			));
			if(!empty($user)){
				$id = $user->id;
			}
		}
		$d['id'] = $id;
		if($this->request->data){
			$this->request->data->pseudo = $user->pseudo;
			$this->request->data->id = $user->id;
			$this->request->data->actif = 1;
			if($this->request->data->password != $user->password){
				$this->request->data->password = sha1($this->request->data->password);
			}else{
				$this->request->data->password === $user->password;
			}
			$this->User->save($this->request->data);
			$this->Session->setFlash('Le contenu a bien été modifié', 'alert alert-success');
		}else{
			$this->request->data = $this->User->findFirst(array(
				'conditions' => array('id'=>$_SESSION['User']->id)
			));
		}
		$this->set($d);
	}


	function logged_loadAvatar($id){
		$this->loadModel('Media');
		$perPage = 1;
		$d['avatar'] = $this->Media->findFirst(array(
			'conditions' => array('user_id' => $id),
			'order' => 'id DESC',
			'limit' => $perPage
		));
		if(!isset($d['avatar']->file)){
			return '<img class="img-rounded" src="/PHP_my_meetic/Blog/img/2014-07/user.png" alt="avatardft" />';
		}else{
			return '<img class="img-rounded" src="/PHP_my_meetic/Blog/img/upload/'.$d['avatar']->file.'" alt="avatar" />';
		}
	}

}
