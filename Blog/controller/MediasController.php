<?php
class MediasController extends Controller{

	function logged_ajout($id = NULL){
		$this->loadModel('Media');
		$user_id = $_SESSION['User']->id;
		if($this->request->data && !empty($_FILES['file']['name'])){
			if(strpos($_FILES['file']['type'], 'image') !== false){
				$dir = WEBROOT.DS.'img'.DS.'upload'.DS.$user_id;
				if(!file_exists($dir)) mkdir($dir,0777);
				move_uploaded_file($_FILES['file']['tmp_name'],$dir.DS.$_FILES['file']['name']);
				$this->Media->save(array(
					'name' => $this->request->data->name,
					'file' => $user_id.'/'.$_FILES['file']['name'],
					'id' => $id,
					'type' => 'img',
					'user_id' => $user_id
				));
				$this->redirect('logged/medias/view/');
			}else{
				$this->Form->errors['file'] = "Le fichier n'est pas une image";
			}
		}
		$d['images'] = $this->Media->find(array(
			'conditions' => array('id' => $id
		)));
		$d['id'] = $id;
		$this->set($d);
	}

	function logged_view(){
		$user_id = $_SESSION['User']->id;
		$this->loadModel('Media');
		$perPage = 1;
		$condition = array('user_id' => $user_id);
		$d['medias'] = $this->Media->find(array(
			'fields' => 'id,name,file,type,user_id',
			'conditions' => array('user_id'=>$user_id),
			'order' => 'id DESC',
			'limit' => $perPage
		));
		$this->set($d);
	}

	function logged_delete($id){
		$user_id = $_SESSION['User']->id;
		$this->loadModel('Media');
		$media = $this->Media->findFirst(array(
			'conditions' => array('id'=>$id)
		));
		unlink(WEBROOT.DS.'img'.DS.'upload'.DS.$media->file);
		$this->Media->delete($id);
		$this->Session->setFlash("Le média a bien été supprimé");
		$this->redirect('logged/medias/view/'.$user_id);
	}

}