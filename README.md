My_Meetic App - built with PHP Object (home-made MVC) - School project
================

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

This application requires:
-------------

- PHP >= 5.4

Getting Started
---------------

  - Clone this repository

  ``` git clone https://github.com/chalasr/PHP_my_meetic.git ```

  - Create database

  ``` Import 'my_meetic.sql' to phpMyAdmin ```

Enjoy !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

[License GPL V3](http://opensource.org/licenses/GPL-3.0)
Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/)
